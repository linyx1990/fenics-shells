# Copyright (C) 2015 Matteo Brunetti
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the Kirchhoff_Love equations on the unit square
with uniform transverse loading with fully clamped boundary conditions.

We use the Continuous/Discontinuous Galerkin formulation to exploit standard
Lagrangian elements for the transverse displacement discretization.
"""

from dolfin import *
from fenics_shells import *

# Create a mesh and define the function space
mesh = UnitSquareMesh(32, 32)

# Problem space (transverse displacement)
element_W = FiniteElement("Lagrange", triangle,  2)
W = FunctionSpace(mesh, element_W)

# Trial and Test function
w = TrialFunction(W)
w_t = TestFunction(W)
w_ = Function(W)

# Material parameters
E = Constant(10920.0)
nu = Constant(0.3)
t = Constant(1.0)

# Boundary conditions
all_boundary = lambda x, on_boundary: on_boundary

# Boundary conditions on displacement
bcs_w = [DirichletBC(W, Constant(0.0), all_boundary)]
# Boundary conditions on rotation
bcs_theta = [DirichletBC(W, Constant(0.0), all_boundary)]

# Rotations
theta_ = grad(w_)
# Curvature
k_ = k(theta_)
# Bending moment
D = (E*t**3)/(12.0*(1.0 - nu**2)) # bending stiffness
M_ = D*((1.0 - nu)*k_ + nu*tr(k_)*Identity(2)) # bending stress tensor
# Bending energy density
psi_b = 1./2.*inner(M_,k_)

# Discontinuos contribution to the elastic energy density
# Stabilization parameter
alpha = cdg_stabilization(E, t)
Pi_cdg = cdg_energy(theta_, M_, alpha, mesh, bcs_theta=bcs_theta, dS=dS)

# Uniform transverse loading and external work
f = Constant(1.0)
W_ext = f*w_*dx

# Total energy
Pi = psi_b*dx - W_ext + Pi_cdg #

# Residuals and Jacobian.
F = derivative(Pi, w_, w_t)
J = derivative(F, w_, w)

# Solve
A, b = assemble_system(J, F, bcs=bcs_w)
solver = LUSolver("mumps")
solver.solve(A, w_.vector(), b)
XDMFFile("output/kirchoff-love-clamped/w.xdmf").write(w_)
