# Copyright (C) 2015 Corrado Maurini
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the Kirchhoff_Love equations on the unit square
with uniform transverse loading and simply supported boundary conditions for an
antisymmetric [0/90] composite.

A reference solution is provided in Table 7.2.3 p.387 of

[1] Reddy, Mechanics of Laminated Composites Plates and Shells, CRC Press, Boca
    Raton (1997),

The reference value for the nomalized transverse displacement in the center is
wbar = 1.6955, where wbar = w_center*(E2*h*3)/(a**4*q0)*(10**2) and h is the
total thickness, a is the side of the square, and q0 is the intensity of the
uniform loading.

We use the Continuous/Discontinuous Galerkin formulation to exploit standard
Lagrangian elements for the transverse displacement discretization.
"""

from dolfin import *
from fenics_shells import *
#import laminates
import numpy as np

# Define relevant parameters and parse from command-line
user = Parameters("user")
user.add("thickness", .01)
user.add("nelements", 25)
user.add("interpolation_u", 3)
user.add("interpolation_w", 3)
try:
    parameters.add(user)
except:
    pass
parameters.parse()

p_user = parameters["user"]
# Create a mesh and define the function space
mesh = UnitSquareMesh(p_user.nelements, p_user.nelements)
mesh.init()

# Problem space (composed of in plane and transverse displacement)
element_W = FiniteElement("Lagrange", triangle,  p_user.interpolation_w)
element_U = VectorElement("Lagrange", triangle, p_user.interpolation_u)
element_UW = MixedElement([element_U, element_W])
UW = FunctionSpace(mesh, element_UW)
# Trial and Test function
uw_test = TestFunction(UW)
uw_trial = TrialFunction(UW)
uw = Function(UW)
(u, w) = split(uw)

# Domains for boundary conditions
all_boundary = lambda x, on_boundary: on_boundary
leftright = lambda x, on_boundary: (near(x[0], 0.) or near(x[0], 1.)) and on_boundary
topbottom = lambda x, on_boundary: (near(x[1], 0.) or near(x[1], 1.)) and on_boundary

# Boundary conditions on displacement (ss1 in Reddy, see figure 7.2.1, pag. 379)
bcs_w = [DirichletBC(UW.sub(1), Constant(0.0), all_boundary),
				   DirichletBC(UW.sub(0).sub(1), Constant(0.0), leftright),
				   DirichletBC(UW.sub(0).sub(0), Constant(0.0), topbottom)]

# Boundary conditions on rotation (normal rotation in free)
bcs_theta = []

# Kirchhoff kinematics
theta = kirchhoff_love_theta(w)
e_ = e(u)
k_ = k(theta)

# Properties of the composite
h = Constant(p_user.thickness) # total thickness
thetas = [0., np.pi/2.] # orientation of the layers
n_layers= len(thetas) # number of layers
hs = h*np.ones(n_layers)/n_layers # thickness of the layers (assuming they are equal)
E2 = Expression("1.") # reference Young modulus

# Calculate the laminates matrices (ufl matrices)
A, B, D = laminates.ABD(25.*E2, E2, 0.5*E2, 0.25, hs, thetas)

# Define the energy using Voigt notation
kv = strain_to_voigt(k_)
ev = strain_to_voigt(e_)
Pi_elastic = (.5*dot(A*ev, ev) + .5*dot(D*kv, kv) + dot(B*kv, ev)) * dx

# Discontinuos contribution to the elastic energy
Mv = D * kv + B * ev # Bending moment in voigt notation
M_ = stress_from_voigt(Mv) # Convertion to tensor notation

# Set the stabilization constant alpha as the average of the bending stiffnesses E1 and E2
# (More correctly, a norm of D shuld be used)
alpha = (D[0,0] + D[1,1])/2.
Pi_cdg = cdg_energy(theta, M_, alpha, mesh, bcs_theta)

# Uniform transverse loading and external work
q0 =1.
f = Constant(q0)
external_work = f*w*dx

# Total energy
Pi = Pi_elastic + Pi_cdg - external_work

# Extract linear and bilinear form of the derivarive (linear problem)
Pi_uw = derivative(Pi, uw, uw_test)
F = replace(Pi_uw, {uw: uw_trial})
a, L = lhs(F), rhs(F)

# Solve
A, b = assemble_system(a, L, bcs=bcs_w)
#solver = LUSolver("mumps")
solver = LUSolver("umfpack")
solver.solve(A, uw.vector(), b)
h0 = p_user.thickness
(u, w) = uw.split(deepcopy = True)
wbar = w(.5,.5)*(h0**3)*(10**2)/q0

# reference solution from Reddy 1997
w_refefence = 1.6955
print "w_center/w_reference = ", wbar/w_refefence

# Save and plot
#File("output_laminate/w.xdmf") << w
#File("output_laminate/w.xdmf") << u
plot(w, interactive = True)
plot(u, interactive = True)
