# Copyright (C) 2016 Matteo Brunetti, Jack S. Hale.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.
 
"""
Shell
"""

from dolfin import *
from fenics_shells import *

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"
parameters["form_compiler"]["quadrature_degree"] = 2
import numpy as np
parameters.form_compiler.quadrature_degree =  2

# Define the mesh
Ly = 3.048
P1, P2 = Point(-np.pi/2., 0.), Point(np.pi/2., Ly)
# mesh = RectangleMesh(P1, P2, 43, 43, "left")
mesh = RectangleMesh(P1, P2, 23, 23, "left")
# import mshr as mr
# dom = mr.Rectangle(P1,P2)
# mesh = mr.generate_mesh(dom, 20)

h_max = mesh.hmax()
h_min = mesh.hmin()


# In-plane displacements, rotations, out-of-plane displacements
# shear strains and Lagrange multiplier field.
element = MixedElement([VectorElement("Lagrange", triangle, 1),
                        VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 1),
                        FiniteElement("N1curl", triangle, 1),
                        RestrictedElement(FiniteElement("N1curl", triangle, 1), "edge")])

# Define the element and the Function Space
U = ProjectedFunctionSpace(mesh, element, num_projected_subspaces=2)
U_F = U.full_space
U_P = U.projected_space

# Define the Trial and Test functions
u, u_t, u_ = TrialFunction(U_F), TestFunction(U_F), Function(U_F)
v_, theta_, w_, Rgamma_, p_ = split(u_)
z_ = as_vector([v_[0], v_[1], w_])

# Define the boundary conditions
clamped_boundary = lambda x, on_boundary: x[1] <= DOLFIN_EPS and on_boundary
left_boundary = lambda x, on_boundary: abs(x[0] + np.pi/2.) <= DOLFIN_EPS and on_boundary
right_boundary = lambda x, on_boundary: abs(x[0] - np.pi/2.) <= DOLFIN_EPS and on_boundary

# Define the boundary conditions
rho = 1.016  # radius
theta_1 = Constant(0.0)
theta_2 = Expression("x[0]", r=rho)
# - clamped boundary conditions
bc_v = DirichletBC(U.sub(0), Constant((0.0,0.0)), clamped_boundary)
bc_theta1 = DirichletBC(U.sub(1).sub(0), theta_1, clamped_boundary)
bc_theta2 = DirichletBC(U.sub(1).sub(1), theta_2, clamped_boundary)
bc_w = DirichletBC(U.sub(2), Constant(0.0), clamped_boundary)
# - lateral edges boundary conditions
bc_wleft = DirichletBC(U.sub(2), Constant(0.0), left_boundary)
bc_wright = DirichletBC(U.sub(2), Constant(0.0), right_boundary)
bc_theta1left = DirichletBC(U.sub(1).sub(0), theta_1, left_boundary)
bc_theta2left = DirichletBC(U.sub(1).sub(1), theta_2, left_boundary)
bc_theta1right = DirichletBC(U.sub(1).sub(0), theta_1, right_boundary)
bc_theta2right = DirichletBC(U.sub(1).sub(1), theta_2, right_boundary)

# - boundary conditions
# bcs = [bc_v, bc_w, bc_theta1, bc_theta2, bc_wleft, bc_wright, bc_theta1left, bc_theta2left, bc_theta1right, bc_theta2right]
bcs = [bc_v, bc_w, bc_theta1, bc_theta2, bc_wleft, bc_wright, bc_theta2left, bc_theta2right]
# bcs = [bc_v, bc_w, bc_theta1, bc_theta2, bc_wleft, bc_wright]

# Define the nonlinear Naghdi strain measures
## TODO these expression can be computed automatically from the parametrization
acov = Expression((('r*r','0.'), ('0.','1.')), degree=2, r=rho)
bcov = Expression((('-r','0.'), ('0.','0.')), degree=2, r=rho)
deta = Expression("r*r", degree=2, r=rho)
grad0 = Expression((('r*cos(x[0])','0.'), ('0.','1.'), ('-r*sin(x[0])','0.')), degree=2, r=rho)

# Director vector
d = lambda theta: as_vector([sin(theta[1])*cos(theta[0]), -sin(theta[0]), cos(theta[1])*cos(theta[0])])
# Deformation gradients
F = lambda u: grad0 + grad(u)
# Stretching tensor (1st Naghdi strain measure)
G = lambda F: 0.5*(F.T*F - acov)
# Curvature tensor (2nd Naghdi strain measure)
K = lambda F, d: -0.5*(F.T*grad(d) + grad(d).T*F) - bcov
# Shear strain vector (3rd Naghdi strain measure)
g = lambda F, d: F.T*d

# Define the material parameters
Y, nu = 2.0685E7, 0.3
mu = Y/(2.0*(1.0 + nu))
lb = 2.0*mu*nu/(1.0 - 2.0*nu)
thickness = 0.03 
eps = Constant(0.5*thickness) # half-thickness

# Define Dual Stress Tensor (constitutive law)
A1111 = Expression('(8.*mu*(lb + mu))/(r*r*r*r*(lb + 2.*mu))', degree=2, mu=mu, lb=lb, r=rho)
A1122 = Expression('(4.*mu*lb)/(r*r*(lb + 2.*mu))', degree=2, mu=mu, lb=lb, r=rho)
A2222 = Expression('(8.*mu*(lb + mu))/(lb + 2.*mu)', degree=2, mu=mu, lb=lb, r=rho)
A1212 = Expression('(2.0*mu)/(r*r)', degree=2, mu=mu, lb=lb, r=rho)
A = as_matrix([[A1111, A1122, 0.],[A1122, A2222, 0.],[0., 0., A1212]])

#TODO this can be computed from the contravariant component of the metric
F11 = Expression('r*r', r=rho) 

# - Define the membrane stress tensor
def S(X):
    Svec = A*as_vector([X[0,0], X[1,1], X[0,1]])
    return as_matrix([[Svec[0], Svec[2]], [Svec[2], Svec[1]]])

# - Define the shear stress vector
def T(X):
    return as_vector([X[0]/F11, X[1]])

# Define the External Work
f = Expression('1.E5*t*t*t*c', c=0.0, t=thickness)
W_ext = f*w_*sqrt(deta)*dx

# Define the energy densities
# psi_G = 0.5*(2.0*eps)*inner(S(G(F(z_))), G(F(z_)))
psi_G = 0.5*eps*inner(S(G(F(z_))), G(F(z_)))
# Bending energy density
# psi_K = 0.5*((2.0*eps)**3/12.0)*inner(S(K(F(z_), d(theta_))), K(F(z_), d(theta_)))
psi_K = 0.5*(eps**3/3.0)*inner(S(K(F(z_), d(theta_))), K(F(z_), d(theta_)))
# Shear energy density
psi_g = 0.5*(2.0*eps)*mu*inner(T(Rgamma_), Rgamma_) # TOCHECK:the 2.0 factor
# Stored strain energy density
psi = (psi_G + psi_K + psi_g)*sqrt(deta)

# Compute the Residual and Jacobian.
# Here we show another way to apply the Duran-Liberman reduction operator,
# through constructing a Lagrangian term L_R.
L_R = inner_e(g(F(z_), d(theta_)) - Rgamma_, p_)
L_el = psi*dx
L = L_el + L_R - W_ext
F = derivative(L, u_, u_t) # First variation of Pi
J = derivative(F, u_, u) # Compute Jacobian of F

# Define initial condition
class InitialConditionsFull(Expression):
    def eval(self, values, x):
        values[0] = 0.0
        values[1] = 0.0
        values[2] = 0.0
        values[3] = 0.0
        values[4] = 0.0
        values[5] = 0.0
        values[6] = 0.0
        values[7] = 0.0
        values[8] = 0.0

    def value_shape(self):
        return (9,)

class InitialConditionsProjected(Expression):
    def eval(self, values, x):
        values[0] = 0.0
        values[1] = 0.0
        values[2] = 0.0
        values[3] = x[0]
        values[4] = 0.0

    def value_shape(self):
        return (5,)

u_init = InitialConditionsFull()
u_.interpolate(u_init)
u_p_ = Function(U_P)
u_p_init = InitialConditionsProjected()
u_p_.interpolate(u_p_init)

# Define nonlinear problem to apply the point load
class ProjectedNonlinearProblem(NonlinearProblem):
    def __init__(self, U_P, F, u_f_, u_p_, bcs=None, J=None):
        """
        Allows the solution of non-linear problems using the projected_assemble
        routines. Can be used with any of the built-in non-linear solvers
        in DOLFIN.

        Args:
            
        """
        NonlinearProblem.__init__(self)
        
        self.U_P = U_P
        self.U_F = U_P.full_space
        self.F_form = F
        self.J_form = J
        self.u_f_ = u_f_
        self.u_p_ = u_p_
        self.bcs = bcs

        self.x_p_prev = u_p_.vector().copy()
        self.x_p_prev.zero()
        self.dx_f = Function(self.U_F)

        self.P = 0.0

    def form(self, A, b, x_p):
        # To do the reconstruction of the increment on the full space, we need
        # the increment on the projected space. This is not accessible through
        # the current DOLFIN interfaces. So, we have to recalculate the
        # projected increment, reconstruct the full increment, then perform
        # the Newton step on the full space.
        
        # Recalculate dx_p. Was available inside NonlinearSolver, but not
        # accessible here.
        dx_p_vector = self.x_p_prev - x_p
        dx_p = Function(self.U_P, dx_p_vector)
    
        # Reconstruct according to J == F 
        reconstruct_full_space(self.dx_f, dx_p, self.J_form, self.F_form)
        
        # Newton update
        self.u_f_.vector()[:] -= self.dx_f.vector()

        # Store x_p for use in next Newton iteration
        self.x_p_prev[:] = x_p
        
        # Then as normal...
        assemble(self.U_P, self.J_form, self.F_form, A=A, b=b)
        point_source = PointSource(self.bcs[1].function_space(), Point(0.0, Ly), self.P)
        point_source.apply(b)

        for bc in self.bcs:
            bc.apply(A, b, x_p)

    def F(self, b, x_p):
        pass
    
    def J(self, A, x_p):
        pass


problem = ProjectedNonlinearProblem(U_P, F, u_, u_p_, bcs=bcs, J=J)
solver = NewtonSolver()

# Define solver parameters
prm = solver.parameters
prm['error_on_nonconvergence'] = True
prm['maximum_iterations'] = 50
prm['absolute_tolerance'] = 1E-9
prm['linear_solver'] = "mumps"

output_dir = "output/naghdi-shell-cyl/"
# output_file = XDMFFile(output_dir + "w.xdmf")

w_list = []
P_max = 2000.
continuation_steps = 120 
P_increments = np.linspace(0.0, P_max, continuation_steps)
print P_increments
for P in P_increments:
    problem.P = P
    solver.solve(problem, u_p_.vector())
    v_h, theta_h, w_h, Rgamma_h, p_h = u_.split(deepcopy=True) # extract components
    w_list.append(w_h(0.0, Ly))


# To reconstruct the solution
el = VectorElement("Lagrange", triangle, 1, dim=3)
Space = FunctionSpace(mesh, el)

# TODO this is just to use paraview warp by vector function
x0 = Expression(('r*sin(x[0])-x[0]','0.', 'r*cos(x[0])'),r=rho, element=el)
initial_conf = project(x0, Space)

displacement = as_vector([v_h[0], v_h[1], w_h])
deformed_conf = x0 + displacement
deformed_conf = project(deformed_conf, Space)

file1 = File(output_dir + "initial_configuration.pvd")
file1 << initial_conf

file2 = File(output_dir + "deformed_configuration.pvd")
file2 << deformed_conf