# Copyright (C) 2016 Matteo Brunetti, Jack S. Hale.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.
 
"""This demo program solves the non-linear Reissner-Mindlin-Naghdi equations on
the unit square with uniform transverse loading and fully clamped boundary
conditions, see:

Jeon H.-M., Lee Y., Lee P.-S., Bathe, K. J., The MITC3+ shell element
in geometric nonlinear analysis, Computers & Structures, 2015.

We use the ProjectedNonlinearProblem class along with a Duran-Liberman reduction
operator to project the non-linear 3rd Naghdi shearing strain measure. This results
in a formulation free of shear-locking.
"""

from dolfin import *
from fenics_shells import *

parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["representation"] = "quadrature"
parameters["form_compiler"]["quadrature_degree"] = 2
import numpy as np
parameters.form_compiler.quadrature_degree =  2

# Define the mesh
P1, P2 = Point(-1., -1.), Point(1., 1.)
mesh = RectangleMesh(P1, P2, 15, 15, "crossed")
h_max = mesh.hmax()
h_min = mesh.hmin()

# In-plane displacements, rotations, out-of-plane displacements
# shear strains and Lagrange multiplier field.
element = MixedElement([VectorElement("Lagrange", triangle, 1),
                        VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 1),
                        FiniteElement("N1curl", triangle, 1),
                        RestrictedElement(FiniteElement("N1curl", triangle, 1), "edge")])

# Define the element and the Function Space
U = ProjectedFunctionSpace(mesh, element, num_projected_subspaces=2)

U_F = U.full_space
U_P = U.projected_space

# Define the Trial and Test functions
u, u_t, u_ = TrialFunction(U_F), TestFunction(U_F), Function(U_F)
v_, theta_, w_, Rgamma_, p_ = split(u_)
z_ = as_vector([v_[0], v_[1], w_])

# Define the boundary conditions
all_boundary = lambda x, on_boundary: on_boundary
bc_v = DirichletBC(U.sub(0), Constant((0.0,0.0)), all_boundary)
bc_theta = DirichletBC(U.sub(1), Constant((0.0,0.0)), all_boundary)
bc_w = DirichletBC(U.sub(2), Constant(0.0), all_boundary)
bcs = [bc_v, bc_theta, bc_w]

# Define the nonlinear Naghdi strain measures
# Director vector
d = lambda theta: as_vector([sin(theta[1])*cos(theta[0]), -sin(theta[0]), cos(theta[1])*cos(theta[0])])
# Deformation gradients
F = lambda u: as_tensor([[1.0, 0.0],[0.0, 1.0],[Constant(0), Constant(0)]]) + grad(u)
# Stretching tensor (1st Naghdi strain measure)
G = lambda F0: 0.5*(F0.T*F0 - Identity(2))
# Curvature tensor (2nd Naghdi strain measure)
K = lambda F0, d:  0.5*(F0.T*grad(d) + grad(d).T*F0)
# Shear strain vector (3rd Naghdi strain measure)
g = lambda F0, d: F0.T*d

# Define the material parameters
Y, nu = Constant(1.7472E3), Constant(0.3) 
mu = Y/(2.0*(1.0 + nu))
lb = 2.0*mu*nu/(1.0 - 2.0*nu)
thickness = 1E-4
eps = Constant(0.5*thickness) # (Half)-Thickness parameter

# Define Dual Stress Tensor (constitutive law)
S = lambda X: 2.0*mu*X + ((2.0*mu*lb)/(2.0*mu + lb))*tr(X)*Identity(2)

# Define the External Work
f = Expression('1.E5*t*t*t*c', c=1.0, t=thickness)
W_ext = f*w_*dx

# Define the energy densities
# Membrane energy density
psi_G = inner(S(G(F(z_))), G(F(z_)))
# Bending energy density
psi_K = ((2.0*eps)**2/12.0)*inner(S(K(F(z_), d(theta_))), K(F(z_), d(theta_)))
# Shear energy density
psi_g = 2.0*mu*inner(Rgamma_, Rgamma_) # TOCHECK:the 2.0 factor
# Stored strain energy density
psi = 0.5*(psi_G + psi_K + psi_g) 

# Compute the Residual and Jacobian.
# Here we show another way to apply the Duran-Liberman reduction operator,
# through constructing a Lagrangian term L_R.
L_R = inner_e(g(F(z_), d(theta_)) - Rgamma_, p_)
L_el = psi*dx
L = L_el + L_R - W_ext
F = derivative(L, u_, u_t) # First variation of Pi
J = derivative(F, u_, u) # Compute Jacobian of F

u_p_ = Function(U_P)
problem = ProjectedNonlinearProblem(U_P, F, u_, u_p_, bcs=bcs, J=J)
solver = NewtonSolver()

output_dir = "output/naghdi-multipliers-clamped/"
output_file = XDMFFile(output_dir + "w.xdmf")

# Solution
w_ls = []
fmax = 1.0
f_incr = np.linspace(0.0, fmax, 10)
for j in f_incr:
    f.c = j
    solver.solve(problem, u_p_.vector())
    v_h, theta_h, w_h, Rgamma_h, p_h = u_.split(deepcopy=True) # extract components
    output_file.write(w_h)
