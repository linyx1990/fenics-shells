# Copyright (C) 2016 Matteo Brunetti.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.
 
"""This demo program solves the Reissner-Mindlin-Naghdi equations.

Implement here the discretisation proposen by Arnold and Brezzi:
MATHEMATICS OF COMPUTATION, Volume 66, Number 217, January 1997, Pages 1-14
https://www.ima.umn.edu/~arnold//papers/shellelt.pdf
"""

import os
import sys
from dolfin import *
from fenics_shells import *

import numpy as np
import mshr
parameters.form_compiler.quadrature_degree = 4
parameters["form_compiler"]["representation"] = "uflacs"

args = [sys.argv[0]] + """
                       --petsc.snes_max_it 100
                       --petsc.snes_atol 1.0e-10
                       --petsc.snes_rtol 1.0e-10
                       --petsc.snes_monitor
                       --petsc.snes_converged_reason

                       --petsc.ksp_type preonly
                       --petsc.pc_type lu
                       --petsc.pc_factor_mat_solver_package mumps
                       """.split()
parameters.parse(args)

# Define the mesh
comm = mpi_comm_world()
mesh = UnitDiscMesh(comm, 12, 1, 2)
hmax = mesh.hmax()

# Define the element
lagr3D = VectorElement("Lagrange", triangle, degree = 1, dim=3)
lagr2D = VectorElement("Lagrange", triangle, degree = 1, dim=2)
bubble3D = VectorElement("B", triangle, degree = 3, dim=3)
bubble2D = VectorElement("B", triangle, degree = 3, dim=2)

element_z_k1 = EnrichedElement(lagr3D, bubble3D)
element_theta_k1 = EnrichedElement(lagr2D, bubble2D)
element = MixedElement([element_z_k1, element_theta_k1])

# Define the Function Space
U = FunctionSpace(mesh, element)
tfs = TensorFunctionSpace(mesh, 'DG', 0)
u = Function(U)
u_trial, u_t = TrialFunction(U), TestFunction(U)
z, theta = split(u)
z_t, theta_t = split(u_t)

# Define the geometric and material parameters
c_0 = Constant(0)
c_I = Constant(0)
E = Constant(1.0)
nu = Constant(0.3)
t = Constant(1E-2)
beta = Constant(0.99)
gamma_iso = (1-nu**2/beta)/(2*(1+nu))
gamma_a = Constant(2.3)*gamma_iso # rho*(1-nu^2/beta)
cs = 0.0516 # curvature scaling (FIXME: put here explicit function of t and geometry)

# Define the Target curvature
k0x, k0y = c_0, c_0
kIx, kIy = c_I, c_I
k_0 = cs*as_tensor([[k0x, 0.0], [0.0, k0y]]) # initial curvature
#k_I = cs*as_tensor([[kIx, 0.0], [0.0, kIy]]) # inelastic curvature
k_I = as_tensor(Expression((("cs*kIx","0.0"),("0.0","cs*kIy")), cs=cs, kIx=0.0, kIy=0.0))
k_T = k_I + k_0 # target curvature

# Define the Target metric
x = SpatialCoordinate(mesh)
e_Tx = -k0x*k0y*x[1]**2/2.
e_Ty = -k0x*k0y*x[0]**2/2.
e_T = (cs**2)*as_tensor([[e_Tx, 0.0], [0.0, e_Ty]])

# Define the Naghdi strain measures
#~ Director vector
d_naghdi = lambda theta: as_vector([sin(theta[1])*cos(theta[0]), -sin(theta[0]), cos(theta[1])*cos(theta[0])])
# Deformation gradient
F_naghdi = lambda displacement: as_tensor([[1.0, 0.0],[0.0, 1.0],[Constant(0), Constant(0)]]) + grad(displacement)
# Stretching tensor (1st Naghdi strain measure)
e_naghdi = lambda F0: 0.5*(F0.T*F0 - Identity(2))
# Curvature tensor (2nd Naghdi strain measure)
k_naghdi = lambda F0, d: 0.5*(F0.T*grad(d) + grad(d).T*F0)
# Shear strain vector (3rd Naghdi strain measure)
g_naghdi = lambda F0, d: F0.T*d

# Define the Kinematics
F = F_naghdi(z)
d = d_naghdi(theta)
e_eff = e_naghdi(F) - e_T
k_eff = k_naghdi(F, d) - k_T
g_eff = g_naghdi(F, d)

# Define the Constitutive properties
EI_eq = (E*t**3)/(12.0*(1.0 - nu**2))
ES_eq = (E*t)/((1.0 - nu**2))
GS_eq = E*t/(2*(1+nu))
es = 1 # EI_eq/(cs**2) # scaling factor for the stiffness (FIXME: think better nondimensional form)
A = es*ES_eq*(as_matrix([[1.,nu,0],[nu,beta,0.],[0.,0.,gamma_a]]))
D = es*EI_eq*(as_matrix([[1.,nu,0],[nu,beta,0.],[0.,0.,gamma_a]]))

# Define the Generalized forces
M_voigt = D*strain_to_voigt(k_eff)
N_voigt = A*strain_to_voigt(e_eff)
N = stress_from_voigt(N_voigt) # membrane stress
M = stress_from_voigt(M_voigt) # bending moment
T = es*GS_eq*g_eff # shear stress

# Define the energies
psi_m = .5*inner(N, e_eff) # Membrane energy density
psi_b = .5*inner(M, k_eff) # Bending energy density
psi_s = .5*inner(T, g_eff) # Shear energy density

# Define the Arnold & Brezzi "selective reduced integration"
C0_AB = Constant(1.) # This is the c_0 in the paper, set to the size of the shell
dx_h = dx(metadata={'quadrature_degree': 1}) # reduced integration
energy = psi_b*dx + (C0_AB*t**2)*psi_s*dx + (1-C0_AB*t**2)*psi_s*dx_h + (C0_AB*t**2)*psi_m*dx + (1-C0_AB*t**2)*psi_m*dx_h
energy = Constant(1e5)*energy

# Define the residual
Pi = energy
dPi = derivative(Pi, u, u_t)
J = derivative(dPi, u, u_trial)

# Define the boundary conditions
center = lambda x, on_boundary: x[0]**2 + x[1]**2 < (1.5*hmax)**2
bc_z = DirichletBC(U.sub(0), project(Constant((0.0,0.0,0.0)),U.sub(0).collapse()), center)
bc_a = DirichletBC(U.sub(1), project(Constant((0.0,0.0)),U.sub(1).collapse()), center)
bcs = [bc_z, bc_a]

# Initial guess
init = Function(U)
u.assign(init)

# Solver settings
problem = NonlinearVariationalProblem(dPi, u, bcs, J = J)
solver = NonlinearVariationalSolver(problem)
solver.parameters.nonlinear_solver = 'snes'
# solver.parameters.snes_solver.linear_solver =  'umfpack'
# solver.parameters.snes_solver.maximum_iterations =  50

output_dir = "output/naghdi-arnoldbrezzi-mansfield/"
import os
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# loadings = np.linspace(0.4, 2, 121)
loadings = np.linspace(0.4, 3.5, 20)

# Solution
kx = []
ky = []
kxy = []
ls_load = []

# D = VectorFunctionSpace(mesh, 'CG', 2, dim=3)
fid = File(output_dir + "solution.pvd")
for i in loadings:

    k_I.kIx = i
    k_I.kIy = i

    solver.solve()

    z_h, theta_h = u.split(deepcopy=True)
    k = k_naghdi(F_naghdi(z_h), d_naghdi(theta_h))
    K_h = project(k, tfs)

    Kxx = assemble(K_h[0,0]*dx)/(2*pi)
    Kxy = assemble(K_h[0,1]*dx)/(2*pi)
    Kyy = assemble(K_h[1,1]*dx)/(2*pi)
    kx.append(Kxx)
    ky.append(Kyy)
    kxy.append(Kxy)

    disp = project(z_h, FunctionSpace(mesh, lagr3D))
    disp.rename("disp", "disp") #see the QA reported below.
    fid << disp, i

np.savetxt(output_dir + "kx.csv", kx)
np.savetxt(output_dir + "ky.csv", ky)
np.savetxt(output_dir + "kT.csv", loadings)