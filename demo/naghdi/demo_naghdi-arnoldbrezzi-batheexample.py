# Copyright (C) 2016 Matteo Brunetti.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the Reissner-Mindlin-Naghdi equations.

Implement here the discretisation proposen by Arnold and Brezzi:
MATHEMATICS OF COMPUTATION, Volume 66, Number 217, January 1997, Pages 1-14
https://www.ima.umn.edu/~arnold//papers/shellelt.pdf
"""

import os
import sys
from dolfin import *
from fenics_shells import *

import numpy as np
import mshr
parameters.form_compiler.quadrature_degree = 4
parameters["form_compiler"]["representation"] = "uflacs"
dolfin.parameters["num_threads"] = 0
Add user parameters, parse from command line, and save to file
user_par = Parameters("user")
user_par.add("ndiv", 60)
user_par.add("stab", 1.)
parameters.add(user_par)
parameters.parse()
user_par = parameters.user
if mpi_comm_world().rank == 0:
    File("parameters.xml") << parameters
    File("user_parameters.xml") << user_par

# Define the mesh
Ly = 3.048
P1, P2 = Point(-np.pi/2., 0.), Point(np.pi/2., Ly)
# mesh = RectangleMesh(P1, P2, 40, 40, "left")
import mshr as mr
dom = mr.Rectangle(P1,P2)
mesh = mr.generate_mesh(dom, user_par.ndiv)
h_max = mesh.hmax()
h_min = mesh.hmin()

# Define the element
lagr = FiniteElement("Lagrange", triangle, degree = 1)
bubble = FiniteElement("B", triangle, degree = 3)
en_element = lagr + bubble # enriched element 
mixed_element = MixedElement(3*[en_element] + 2*[en_element])

# Define the function space
U = FunctionSpace(mesh, mixed_element)
u = Function(U)
u_trial, u_t = TrialFunction(U), TestFunction(U)
z1, z2, z3, th1, th2 = split(u) # components
z1_t, z2_t, z3_t, th1_t, th2_t = split(u_t)
z = as_vector([z1, z2, z3]) # displacement vector
theta = as_vector([th1, th2]) # rotation vector
z_t = as_vector([z1_t, z2_t, z3_t])
theta_t = as_vector([th1_t, th2_t])

# Define the geometric and material parameters
rho = 1.016  # radius
Y, nu = 2.0685E7, 0.3
mu = Y/(2.0*(1.0 + nu))
lb = 2.0*mu*nu/(1.0 - 2.0*nu)
thickness = 0.03
t = Constant(thickness) 

# Define the initial configuration
VSpace = VectorFunctionSpace(mesh, 'CG', 2, dim=3)
X0 = Expression(('r*sin(x[0])','x[1]','r*cos(x[0])'), r=rho, degree = 4)
X0 = interpolate(X0, VSpace)

# Compute the I fundamental form
gradX0 = grad(X0) 
acov = gradX0.T*gradX0 # covariant components of I fundamental form
acontra = inv(acov) # contravariant components of II fundamental form
deta = det(acov)

# Compute the II fundamental form
N0 = cross(X0.dx(0), X0.dx(1))/sqrt(dot(cross(X0.dx(0), X0.dx(1)), cross(X0.dx(0), X0.dx(1)))) # reference surface normal
bcov = -0.5*(grad(X0).T*grad(N0) + grad(N0).T*grad(X0)) # covariant components of II fundamental form

# Define the director vector parametrization
d_naghdi = lambda theta: as_vector([sin(theta[1])*cos(theta[0]), -sin(theta[0]), cos(theta[1])*cos(theta[0])])

# Define the Naghdi strain measures
F_naghdi = lambda u: gradX0 + grad(u) # Deformation gradients
e_naghdi = lambda F: 0.5*(F.T*F - acov) # Stretching tensor (1st Naghdi strain measure)
k_naghdi = lambda F, d: -0.5*(F.T*grad(d) + grad(d).T*F) - bcov # Curvature tensor (2nd Naghdi strain measure)
g_naghdi = lambda F, d: F.T*d # Shear strain vector (3rd Naghdi strain measure)

# Define the kinematics
F = F_naghdi(z)
d = d_naghdi(theta)
e_eff = e_naghdi(F)
k_eff = k_naghdi(F, d)
g_eff = g_naghdi(F, d)

# Define the constitutive properties and the constitutive law
EI_eq = (t**3/24.0)
ES_eq = t/2.0
GS_eq = t*mu
i, j, k, l = Index(), Index(), Index(), Index()
Am = as_tensor((((4.0*lb*mu)/(lb + 2.0*mu))*acontra[i,j]*acontra[k,l] + 2.0*mu*(acontra[i,k]*acontra[j,l] + acontra[i,l]*acontra[j,k])),[i,j,k,l])
es = 1 # EI_eq/(cs**2) # scaling factor for the stiffness (FIXME: think better nondimensional form)
A = es*ES_eq*Am
D = es*EI_eq*Am
G = es*GS_eq*acontra

# Define the generalized forces
N = as_tensor((A[i,j,k,l]*e_eff[k,l]),[i, j])
M = as_tensor((D[i,j,k,l]*k_eff[k,l]),[i, j])
T = as_tensor((G[i,j]*g_eff[j]), [i])

# Define the energy densities
psi_b = .5*inner(M, k_eff) # Bending energy density
psi_m = .5*inner(N, e_eff) # Membrane energy density
psi_s = .5*inner(T, g_eff) # Shear energy density

# Define the energies
Jb = psi_b*sqrt(deta)*dx # Bending energy
Jm = psi_m*sqrt(deta)*dx # Membrane energy
Js = psi_s*sqrt(deta)*dx # Shear energy

# Define the Arnold & Brezzi "selective reduced integration"
# C0_AB = Constant(Ly) # This is the c_0 in the paper, set to the size of the shell
C0_AB = Constant(user_par.stab/mesh.hmax())  # This is the c_0 in the paper, set to the size of the shell
dx_h = dx(metadata={'quadrature_degree': 1}) # reduced integration
Js_h = psi_s*sqrt(deta)*dx_h # Reduced shear energy term
Jm_h = psi_m*sqrt(deta)*dx_h # Reduced membrane energy term
energy = Jb + (C0_AB*t**2)*Js + (1.0 - C0_AB*t**2)*Js_h + (C0_AB*t**2)*Jm + (1.0 - C0_AB*t**2)*Jm_h # Arnold & Brezzi Partial Selective Reduced energy
energy = Constant(1.)*energy

# Define the initial guess
NSpace = FunctionSpace(mesh,'CG', 2)
nx, ny, nz = project(N0[0], NSpace), project(N0[1], NSpace), project(N0[2], NSpace)
th1_init = Expression("atan2(-ny, sqrt(pow(nx,2) + pow(nz,2)))", nx=nx, ny=ny, nz=nz, degree=1) # from the director parametrization
th2_init = Expression("atan2(nx,nz)", nx=nx, nz=nz, degree=1)
init = project(Expression(("0.0", "0.0", "0.0", "th1", "th2"), th1=th1_init, th2=th2_init, degree=1), U)
u.assign(init)

##########################################
# Computing the error in the natural configuration energies
u0 = as_vector([u[0], u[1], u[2]])
r0 = as_vector([u[3], u[4]])
d0 = d_naghdi(r0)
F0 = F_naghdi(u0)
e0 = e_naghdi(F0)
k0 = k_naghdi(F0,d0)
g0 = g_naghdi(F0,d0)
psi_m0 = .5*inner(as_tensor((A[i,j,k,l]*e0[k,l]),[i, j]), e0)
psi_b0 = .5*inner(as_tensor((D[i,j,k,l]*k0[k,l]),[i, j]), k0)
psi_s0 = .5*inner(as_tensor((G[i,j]*g0[j]), [i]), g0)
# e0_norm = sqrt(assemble(inner(e0, e0)*dx))
# k0_norm = sqrt(assemble(inner(k0, k0)*dx))
# g0_norm = sqrt(assemble(inner(g0, g0)*dx))
Pi_m0 = assemble(psi_m0*sqrt(deta)*dx)
Pi_b0 = assemble(psi_b0*sqrt(deta)*dx)
Pi_s0 = assemble(psi_s0*sqrt(deta)*dx)
d0_er_norm = sqrt(assemble(dot(cross(d0,N0), cross(d0,N0))*dx))
print '---- Error in the natural configuration energies ----'
print 'initial error on the director wrt to the surface normal: %s' %d0_er_norm
print 'initial membrane energy: %s' %Pi_m0
print 'initial bending energy: %s' %Pi_b0
print 'initial shearing energy: %s' %Pi_s0
print '------------------------------------------------------'
##########################################

# Define the boundaries
clamped_boundary = lambda x, on_boundary: x[1] <= DOLFIN_EPS and on_boundary
left_boundary = lambda x, on_boundary: abs(x[0] + np.pi/2.) <= DOLFIN_EPS and on_boundary
right_boundary = lambda x, on_boundary: abs(x[0] - np.pi/2.) <= DOLFIN_EPS and on_boundary

# Define the boundary conditions
bc_clamp = DirichletBC(U, project(Expression(("0.0", "0.0", "0.0", "th1", "th2"), th1=th1_init, th2=th2_init, degree = 1), U), clamped_boundary)
bc_wleft = DirichletBC(U.sub(2), project(Constant(0.0), U.sub(2).collapse()), left_boundary)
bc_wright = DirichletBC(U.sub(2), project(Constant(0.0), U.sub(2).collapse()), right_boundary)
bc_thleft = DirichletBC(U.sub(4), project(Expression("th2", th2=th2_init, degree=1), U.sub(4).collapse()), left_boundary)
bc_thright = DirichletBC(U.sub(4), project(Expression("th2", th2=th2_init, degree=1), U.sub(4).collapse()), right_boundary)
bcs = [bc_clamp, bc_wleft, bc_wright, bc_thleft, bc_thright]

# Define the external Work
f =Constant(0.0)
W_ext = f*z3*sqrt(deta)*dx

# Define the residual
Pi = energy - W_ext
dPi = derivative(Pi, u, u_t)
J = derivative(dPi, u, u_trial)

# Apply the point source
class MyNonlinearProblem(NonlinearProblem):
    def __init__(self, L, a, bcs):
        NonlinearProblem.__init__(self)
        self.L = L
        self.a = a
        self.bcs = bcs
        self.P = 0.0

    def F(self, b, x):
        assemble(self.L, tensor=b)

        for bc in self.bcs:
            bc.apply(b, x)

        point_source = PointSource(self.bcs[0].function_space().sub(2), Point(0.0, Ly), self.P)
        point_source.apply(b)

    def J(self, A, x):
        assemble(self.a, tensor=A)

        for bc in self.bcs:
            bc.apply(A, x)


# Define the problem and the solver
problem = MyNonlinearProblem(dPi, J, bcs)
solver = NewtonSolver()

# Set solver parameters
prm = solver.parameters
prm['error_on_nonconvergence'] = False
prm['maximum_iterations'] = 50
prm['absolute_tolerance'] = 1E-9
prm['relative_tolerance'] = 1E-6
prm['linear_solver'] = "mumps"

# Set output directory
output_dir = "output/naghdi-arnoldbrezzi-cylinder/"

# Solve
P_max = 2000
continuation_steps = 100
P_increments = np.linspace(0.0, P_max, continuation_steps)
#Initialization of postprecessing functions
probe = Probes(np.array([0.0, Ly]), u.function_space())
Disp = FunctionSpace(mesh, VectorElement("Lagrange", triangle, 1, dim=3))
x0 = Expression(('r*sin(x[0]) - x[0]','0.', 'r*cos(x[0])'), r=rho, degree = 2)
output_file = XDMFFile(output_dir + "configuration.xdmf")

for (i,P) in enumerate(P_increments):
    problem.P = P
    solver.solve(problem, u.vector())
    # probing the transverse displacement
    probe(u)
    #w_val = probe.get_probes_component_and_snapshot(2,i)
    #print(w_val)
    v1_h, v2_h, w_h, th1_h, th2_h = u.split(deepcopy=True) # extract components
    disp = project(x0 + as_vector([v1_h, v2_h, w_h]), Disp)
    disp.rename("displacement", "displacement")
    output_file.write(disp, P)
    result = probe.array(root=0)
    if mpi_comm_world().rank == 0:
        np.savetxt(output_dir+"probe_arnoldbrezzi_left.csv", result)
        np.savetxt(output_dir+"load_arnoldbrezzi_left.csv", P_increments)
        print("Solved for P = ", P)
    probe.array(filename = output_dir+"arnoldbrezzi_left")