# Copyright (C) 2016 Jack S. Hale.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This program generates both linear and quadratic geometry cylinder meshes
in section 4.5 according to the test problem described in Jeon et al. 
http://dx.doi.org/10.1016/j.compstruc.2014.09.004

Note that the text in Jeon et al. is slightly wrong. As written:

    The length and radius of the half cylinder are L = 0.3048 and R = 1.016.

The text should read:

    The length and radius of the half cylinder are L = 1.016 and R = 0.3048.

Output is written to meshes/ subdirectory in XDMF format, along with
MeshFunction containing with the following size_t data:
    ds(0) - all of boundary,
    ds(1) - bottom supports,
    ds(2) - front circle,
    ds(3) - rear circle.
"""

from dolfin import *

# NOTE: Only uflacs backend supports quadratic geometry.
parameters["form_compiler"]["representation"] = "uflacs"

# Parameters for mesh generation
R = 0.3048 # Radius
z_max = 1.016  # Length of cylinder
psi_max = pi # Extrusion angle

# First we begin by taking the bottom off of a UnitCubeMesh so that we have a
# 2D manifold mesh embedded in 3D.
# TODO: Is there a way to directly lift a UnitSquareMesh to R^3?
mesh = UnitCubeMesh(20, 20, 20)
boundary_mesh = BoundaryMesh(mesh, "exterior")

class BottomSurface(SubDomain):
    def inside(self, x, on_boundary):
        return near(x[2], 0.0)

mesh_flat = SubMesh(boundary_mesh, BottomSurface())

linear_parametric_mesh = Mesh(mesh_flat)

# Warp mesh to match parametric domain [0, L]
linear_parametric_mesh.coordinates()[:, 0] *= z_max
linear_parametric_mesh.coordinates()[:, 1] *= psi_max

# Output the parametric mesh as well.
XDMFFile("meshes/linear-parametric-mesh.xdmf").write(linear_parametric_mesh)

# Make a quadratic (P2) geometry version of the parametric domain
quadratic_parametric_mesh = p_refine(linear_parametric_mesh)

def cylindrical_map(parametric_mesh):
    """Coordinate transformation taking parametric domain to cylinder.
    """
    manifold_mesh = Mesh(parametric_mesh)
    from numpy import sin, cos
    manifold_mesh.coordinates()[:, 0] = R*cos(parametric_mesh.coordinates()[:, 1])
    manifold_mesh.coordinates()[:, 1] = R*sin(parametric_mesh.coordinates()[:, 1])
    manifold_mesh.coordinates()[:, 2] = parametric_mesh.coordinates()[:, 0]
    return manifold_mesh

linear_cylinder = cylindrical_map(linear_parametric_mesh)
quadratic_cylinder = cylindrical_map(quadratic_parametric_mesh)

# Basic checks that we actually get quadratic geometry

# NOTE: Current bug in FFC means that quadrature order detection is broken for
# quadratic geometry. An insufficient quadrature rule will be used. It is necessary
# to prescribe the desired degree (2 for quadratic geometry!).
# See: https://bitbucket.org/fenics-project/ufl/issues/80
from numpy import abs
area_exact = z_max*psi_max*R
area_p1 = assemble(1.0*dx(linear_cylinder, degree=1))
area_p2 = assemble(1.0*dx(quadratic_cylinder, degree=2))
assert(abs(area_p2 - area_exact) < abs(area_p1 - area_exact))  

length_exact = 2.0*(psi_max*R) + 2.0*z_max
length_p1 = assemble(1.0*ds(linear_cylinder, degree=1))
length_p2 = assemble(1.0*ds(quadratic_cylinder, degree=2))
assert(abs(length_p2 - length_exact) < abs(length_p1 - length_exact))  

def mark_boundaries(cylinder_mesh):
    # Mark boundary of the mesh as MeshFunction, saves having to do it
    # in the solver.
    class Bottom(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[1], 0.0) and on_boundary

    class Front(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[2], 0.0) and on_boundary

    class Back(SubDomain):
        def inside(self, x, on_boundary):
            return near(x[2], z_max) and on_boundary

    bottom = Bottom()
    front = Front()
    back = Back()

    boundaries = MeshFunction("size_t", cylinder_mesh, cylinder_mesh.topology().dim() - 1)
    boundaries.set_all(0)
    bottom.mark(boundaries, 1)
    front.mark(boundaries, 2)
    back.mark(boundaries, 3)

    return boundaries

linear_boundaries = mark_boundaries(linear_cylinder)
# TODO: Cannot read the quadratic boundary function in Paraview.
quadratic_boundaries = mark_boundaries(quadratic_cylinder)

# More sanity checks on boundary markers.
ds = Measure("ds", domain=linear_cylinder, subdomain_data=linear_boundaries)
length_front_p1 = assemble(1.0*ds(2, degree=1))

ds = Measure("ds", domain=quadratic_cylinder, subdomain_data=quadratic_boundaries)
length_front_p2 = assemble(1.0*ds(2, degree=2))

length_front_exact = psi_max*R

assert(abs(length_front_p2 - length_front_exact) < abs(length_front_p1 - length_front_exact))

XDMFFile("meshes/linear-cylinder.xdmf").write(linear_cylinder)
XDMFFile("meshes/quadratic-cylinder.xdmf").write(quadratic_cylinder)
XDMFFile("meshes/linear-cylinder-boundaries.xdmf").write(linear_boundaries)
# TODO: Can't read the quadratic MeshFunction back in yet.
XDMFFile("meshes/quadratic-cylinder-boundaries.xdmf").write(quadratic_boundaries)
