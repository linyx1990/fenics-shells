# Copyright (C) 2016 Matteo Brunetti, Jack S. Hale.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.
 
"""This demo program solves the non-linear Reissner-Mindlin-Naghdi equations on
a clamped cylinder subject to a point load at one end. See:

Jeon H.-M., Lee Y., Lee P.-S., Bathe, K. J., The MITC3+ shell element
in geometric nonlinear analysis, Computers & Structures, 2015.

We solve in the parametric domain (z, psi) and use an exact mapping
of the geometry to calculate the inelastic strain measures on
the undeformed surface.

Note that the text in Jeon et al. is slightly wrong. As written:

    The length and radius of the half cylinder are L = 0.3048 and R = 1.016.

The text should read:

    The length and radius of the half cylinder are L = 1.016 and R = 0.3048.

In this initial test we are not using the MITC type reduction operator.
"""
import numpy as np
from dolfin import *
from fenics_shells import *

parameters["form_compiler"]["representation"] = "uflacs"
parameters["form_compiler"]["cpp_optimize"] = True
parameters["form_compiler"]["quadrature_degree"] = 4 

# Parameters for mesh generation
R = 1.016 # Radius
z_max = 3.048  # Length of cylinder
psi_max = pi # Extrusion angle

mesh = UnitSquareMesh(100, 100)
mesh.coordinates()[:, 0] *= z_max
mesh.coordinates()[:, 1] *= psi_max

# In-plane displacements, rotations, out-of-plane displacements
# shear strains and Lagrange multiplier field.
element = MixedElement([VectorElement("Lagrange", triangle, 1),
                        VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 1)])

# Define the element and the Function Space
U = FunctionSpace(mesh, element)
U_F = U 

# Define the Trial and Test functions
u, u_t, u_ = TrialFunction(U_F), TestFunction(U_F), Function(U_F)
v_, theta_, w_ = split(u_)
z_ = as_vector([v_[0], v_[1], w_])

# Define the boundary conditions
# Sides of cylinder
bottom_boundary = lambda x, on_boundary: near(x[1], 0.0) and on_boundary
top_boundary = lambda x, on_boundary: near(x[1], psi_max) and on_boundary
# Back of cylinder
right_boundary = lambda x, on_boundary: near(x[0], z_max) and on_boundary
supported_boundary = lambda x, on_boundary: bottom_boundary(x, on_boundary) or top_boundary(x, on_boundary)
bc_theta = DirichletBC(U.sub(1), Constant((0.0, 0.0)), supported_boundary)
bc_w = DirichletBC(U.sub(2), Constant(0.0), supported_boundary)
bc_clamped = DirichletBC(U, Constant((0.0, 0.0, 0.0, 0.0, 0.0)), right_boundary)
bcs = [bc_w, bc_theta, bc_clamped] 

# Define the nonlinear Naghdi strain measures
# Director vector
d = lambda theta: as_vector([sin(theta[1])*cos(theta[0]), -sin(theta[0]), cos(theta[1])*cos(theta[0])])
# Deformation gradients
F = lambda u: as_tensor([[1, 0],[0, 1], [Constant(0), Constant(0)]]) + grad(u)
# Stretching tensor (1st Naghdi strain measure)
# Subtract inelastic stretching
G = lambda F0: 0.5*(F0.T*F0 - as_tensor([[Constant(1), Constant(0)], [Constant(0), Constant(R**2)]])) 
# Curvature tensor (2nd Naghdi strain measure)
# Subtract inelastic curvature
K = lambda F0, d: 0.5*(F0.T*grad(d) + grad(d).T*F0) - as_tensor([[Constant(0), Constant(0)], [Constant(0), Constant(R)]])
# Shear strain vector (3rd Naghdi strain measure)
# Assume initially 'Koiter-like' shell
g = lambda F0, d: F0.T*d

# Define the material parameters
Y, nu = Constant(2.0685E7), Constant(0.3) 
mu = Y/(2.0*(1.0 + nu))
lb = 2.0*mu*nu/(1.0 - 2.0*nu)
thickness = 0.03 
eps = Constant(0.5*thickness) # (Half)-Thickness parameter

# Define Dual Stress Tensor (constitutive law)
S = lambda X: 2.0*mu*X + ((2.0*mu*lb)/(2.0*mu + lb))*tr(X)*Identity(2)

# Define the External Work
f = Constant(0.0)
W_ext = f*w_*dx

# Define the energy densities
# Membrane energy density
psi_G = (2.0*eps)*inner(S(G(F(z_))), G(F(z_)))
# Bending energy density
psi_K = ((2.0*eps)**3/12.0)*inner(S(K(F(z_), d(theta_))), K(F(z_), d(theta_)))
# Shear energy density
psi_g = 2.0*mu*(2.0*eps)*inner(g(F(z_), d(theta_)), g(F(z_), d(theta_))) # TOCHECK:the 2.0 factor
# Stored strain energy density
psi = 0.5*(psi_G + psi_K + psi_g) 

L_el = Constant(R)*psi*dx
L = L_el - W_ext
F = derivative(L, u_, u_t) # First variation of Pi
J = derivative(F, u_, u) # Compute Jacobian of F

class NaghdiEquation(NonlinearProblem):
    def __init__(self, L, a, bcs):
        NonlinearProblem.__init__(self)
        self.L = L
        self.a = a
        self.bcs = bcs
        self.P = 0.0        

    def form(self, A, b, x):
        assemble_system(self.a, self.L, bcs=self.bcs, A_tensor=A, b_tensor=b)
        point_source = PointSource(self.bcs[2].function_space().sub(2), Point(0.0, psi_max/2.0), self.P) 
        point_source.apply(b)

    def F(self, b, x):
        pass

    def J(self, A, x):
        pass

problem = NaghdiEquation(F, J, bcs)
solver = NewtonSolver()

# Solver parameters
prm = solver.parameters
prm['error_on_nonconvergence'] = False
prm['maximum_iterations'] = 20
prm['linear_solver'] = "mumps"

output_dir = "output/naghdi-cylinder/"
output_file = XDMFFile(output_dir + "w.xdmf")

P_max = -900.0 
continuation_steps = 20 
P_increments = np.linspace(0.0, P_max, continuation_steps)
print P_increments
for P in P_increments:
    problem.P = P
    solver.solve(problem, u_.vector())
    v_h, theta_h, w_h = u_.split(deepcopy=True) # extract components
    output_file.write(w_h)
