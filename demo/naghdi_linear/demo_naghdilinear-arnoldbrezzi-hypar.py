# Copyright (C) 2016 Matteo Brunetti.
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the Reissner-Mindlin-Naghdi linear equations.

Implement here the discretisation proposen by Arnold and Brezzi:
MATHEMATICS OF COMPUTATION, Volume 66, Number 217, January 1997, Pages 1-14
https://www.ima.umn.edu/~arnold//papers/shellelt.pdf
"""

import os
import sys
from dolfin import *
from fenics_shells import *

import numpy as np
import mshr
parameters.form_compiler.quadrature_degree = 4
parameters["form_compiler"]["representation"] = "uflacs"

# Define the mesh
import mshr as mr
# dom = mr.Rectangle(Point(-0.5, -0.5), Point(0.5, 0.5))
# mesh = mr.generate_mesh(dom, 120)
mesh = RectangleMesh(Point(-0.5, -0.5), Point(0.5, 0.5), 120, 120, 'crossed')
h_max = mesh.hmax()
h_min = mesh.hmin()

# Define the initial configuration
VSpace = VectorFunctionSpace(mesh, 'CG', 2, dim=3)
X0 = Expression(('x[0]','x[1]','x[0]*x[0]-x[1]*x[1]'), degree = 4)
X0 = interpolate(X0, VSpace)

# first fundamental form
gradX0 = grad(X0)
acov = gradX0.T*gradX0
acontra = inv(acov)
deta = det(acov)

# second fundamental form
N0 = cross(X0.dx(0), X0.dx(1))/sqrt(dot(cross(X0.dx(0), X0.dx(1)), cross(X0.dx(0), X0.dx(1))))
bcov = -0.5*(grad(X0).T*grad(N0) + grad(N0).T*grad(X0))
gradN0 = grad(N0)

# Define the element (Arnold and Brezzi)
lagr = FiniteElement("Lagrange", triangle, degree = 2)
bubble = FiniteElement("B", triangle, degree = 3)
en_element = lagr + bubble
mixed_element = MixedElement(3*[en_element] + 3*[en_element])

# Define the Function Space
U = FunctionSpace(mesh, mixed_element)
dofs = U.dim()
u = Function(U)
u_trial, u_t = TrialFunction(U), TestFunction(U)
z1, z2, z3, th1, th2, th3 = split(u) # components
z1_t, z2_t, z3_t, th1_t, th2_t, th3_t = split(u_t)
z = as_vector([z1, z2, z3]) # displacement vector
theta = as_vector([th1, th2, th3]) # rotation vector
z_t = as_vector([z1_t, z2_t, z3_t])
theta_t = as_vector([th1_t, th2_t, th3_t])

# Define the geometric and material parameters
Y, nu = 1.0, 0.3
mu = Y/(2.0*(1.0 + nu))
t = 1e-4 # thickness

## Define the linear Naghdi strain measures
# Stretching tensor (1st Naghdi strain measure)
e_naghdi = lambda v: 0.5*(gradX0.T*grad(v) + grad(v).T*gradX0)
# Curvature tensor (2nd Naghdi strain measure)
k_naghdi = lambda v, phi: -0.5*(gradX0.T*grad(phi) + grad(phi).T*gradX0) - 0.5*(gradN0.T*grad(v) + grad(v).T*gradN0)
# Shear strain vector (3rd Naghdi strain measure)
g_naghdi = lambda v, phi: gradX0.T*phi + grad(v).T*N0

# Define the Kinematics
e_eff = e_naghdi(z)
k_eff = k_naghdi(z, theta)
g_eff = g_naghdi(z, theta)

# Define the Constitutive properties and the Constitutive law (TOCHECK, see Le Dret, a posteriori analysis)
i, j, k, l = Index(), Index(), Index(), Index()
Am = as_tensor(((Y/(1.0 - nu*nu))*(acontra[i,k]*acontra[j,l] + nu*acontra[i,j]*acontra[k,l])),[i,j,k,l])
A = Am
D = (1.0/12.0)*Am
G = mu*acontra

# Define the Generalized forces
N = as_tensor((A[i,j,k,l]*e_eff[k,l]),[i, j])
M = as_tensor((D[i,j,k,l]*k_eff[k,l]),[i, j])
T = as_tensor((G[i,j]*g_eff[j]), [i])

# Define the energies
psi_m = .5*inner(N, e_eff) # Membrane energy density
psi_b = .5*inner(M, k_eff) # Bending energy density
psi_s = .5*inner(T, g_eff) # Shear energy density

# Define the penalization term for the tangency condition 
# (see Blouza et al., Two Finite Element Approximations of Naghdi's Shell Model in Cartesian Coordinates, SIAM, 2006)
p_blouza = t
## not intrinsic
energy_blouza = 0.5*(1./p_blouza)*inner(grad(inner(theta, N0)), grad(inner(theta, N0)))*dx
# energy_blouza = 0.5*(1./p_blouza)*(inner(theta, N0)*inner(theta, N0) + inner(grad(inner(theta, N0)), grad(inner(theta, N0))))*dx
## intrinsic
# energy_blouza = 0.5*(1./p_blouza)*inner(dot(acontra, grad(inner(theta, N0))), grad(inner(theta, N0)))*sqrt(deta)*dx

# bending, membrane, shearing energy
J_b = psi_b*sqrt(deta)*dx
J_m = psi_m*sqrt(deta)*dx
J_s = psi_s*sqrt(deta)*dx
# membrane, shearing energy reduced integration
dx_h = dx(metadata={'quadrature_degree': 1}) # reduced integration
J_mh = psi_m*sqrt(deta)*dx_h
J_sh = psi_s*sqrt(deta)*dx_h
# Arnold and Brezzi Partial Selective Reduced energy
alpha = 1.0
kappa = 1.0/t**2
energy_PSR = J_b + alpha*J_s + (kappa - alpha)*J_sh + alpha*J_m + (kappa - alpha)*J_mh
# energy
energy = energy_PSR + energy_blouza

# Define the boundary conditions
left_boundary = lambda x, on_boundary: abs(x[0] + 0.5) <= DOLFIN_EPS and on_boundary

# - clamped boundary conditions
clamp = DirichletBC(U, project(Expression(("0.0", "0.0", "0.0", "0.0", "0.0", "0.0"), degree = 4), U), left_boundary)
bcs = [clamp]

# Define the External Work
body_force = 1.
f = Constant(body_force)
W_ext = f*z3*sqrt(deta)*dx

# Define the residual
Pi = energy - W_ext
dPi = derivative(Pi, u, u_t)
J = derivative(dPi, u, u_trial)

# Solve
Amat, bvec = assemble_system(J, dPi, bcs=bcs)
solver = LUSolver("mumps")
print("Number of dofs is %s", dofs)
solver.solve(Amat, u.vector(), bvec)

ux, uy, uz, thx, thy, thz = u.split(deepcopy=True)

output_dir = "output/naghdilinear-arnoldbrezzi-hypar/"

# Transverse displacement
file = File(output_dir + "mod_uz.pvd")
file << uz

# Initial and deformed configuration
initial_conf = project(X0, VSpace)
displacement = as_vector([ux, uy, uz])
scale_factor = 1e0 # scale factor to visualiza the deformed configuration
deformed_conf = X0 + scale_factor*displacement
deformed_conf = project(deformed_conf, VSpace)
file1 = File(output_dir + "mod_initial_configuration.pvd")
file1 << initial_conf
file2 = File(output_dir + "mod_deformed_configuration.pvd")
file2 << deformed_conf

# Checking the tangency condition
r = as_vector([thx, thy, thz])
tangency_condition = project(dot(r, N0), FunctionSpace(mesh, 'CG', 1))
file3 = File(output_dir + "mod_tangency_condition.pvd")
file3 << tangency_condition
er_tan = norm(project(dot(r,N0), FunctionSpace(mesh,'DG',0)), 'L2')
print("error on tangency condition %s  ", er_tan)

# sys.exit("STOP") 
# Computing energies
bending_energy_density = project(psi_b, FunctionSpace(mesh, 'CG', 1))
membrane_energy_density = project(psi_m, FunctionSpace(mesh, 'CG', 1))
elastic_energy = assemble(energy)
bending_energy = assemble(psi_b*sqrt(deta)*dx)
membrane_energy = assemble(psi_m*sqrt(deta)*dx)
reference_displacement = uz(0.5, 0.0)
file4 = File(output_dir + "mod_bending_energy_density.pvd")
file4 << bending_energy_density
file5 = File(output_dir + "mod_membrane_energy_density.pvd")
file5 << membrane_energy_density

# Store main results
result = [dofs, t, elastic_energy, bending_energy, membrane_energy, reference_displacement]
np.savetxt(output_dir + "hypar_results.csv", result)