# Copyright (C) 2015 Jack S. Hale
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the out-of-plane Reissner-Mindlin equations on the
unit square with uniform transverse loading with fully clamped boundary conditions.

We use the MITC7 projection operator expressed in pure UFL where extra Lagrange
multipliers exist on the edge and in the interior of each element to enforce
the compatibility of the rotations and the reduced rotations.

This is in contrast to the other examples in this directory where the
projected_assemble function is used to eliminate all of the extraneous degrees
of freedom.
"""

from dolfin import *
from fenics_shells import *

import dolfin
assert dolfin.__version__ == "1.5.0", "Does not work with DOLFIN > 1.5.0 because of enriched DOFs issue #69."

# Create a mesh and define the full function space
# and the primal function space
mesh = UnitSquareMesh(32, 32)
# Lagrange problem space (rotations, transverse displacement, reduced
# rotations, facet lagrange multipliers, cell lagrange multipliers)
U_L = MITC7(mesh, space_type="lagrange")

# Material parameters
E = Constant(10920.0)
nu = Constant(0.3)
kappa = Constant(5.0/6.0)
t = Constant(0.001)

# rotations, transverse displacements, transverse shear strain, facet lagrange
# multipliers cell langrange multipliers
u = TrialFunction(U_L)
theta, z, R_theta, p, q = split(u)
# test functions are suffixed with _t
v = TestFunction(U_L)
theta_t, z_t, R_theta_t, p_t, q_t = split(v)
u_ = Function(U_L)

# Then, we define the shear energy in terms of the reduced rotation space. Note
# that we must attach out own measure dx. This is so you can easily define
# multi-material problems if you want to.

# Shear strain on reduced rotations
R_gamma = gamma(R_theta, z)
T = T(R_gamma, E=E, nu=nu, kappa=kappa, t=t)
Pi_s = shear_energy(R_gamma, T)*dx
Pi_s = action(Pi_s, u_)
# residual
F_s = derivative(Pi_s, u_, v)
# Jacobian. We will assemble this later.
a_s = derivative(F_s, u_, u)

# The bending energy is defined in terms of the standard rotation space.
k = k(theta)
M = M(k, E=E, nu=nu, t=t)
Pi_b = bending_energy(k, M)*dx
Pi_b = action(Pi_b, u_)
F_b = derivative(Pi_b, u_, v)
a_b = derivative(F_b, u_, u)

# The reduction operator. Details about how this works can be found in the
# documentation of the class MITC7. Here a measure is attached on the entire mesh,
# on the assumption that you wish to cure locking everywhere!
R = U_L.reduction_operator(u, v)

# Uniform transverse loading
f = Constant(1.0)
L = t**3*f*v[2]*dx

# Now boundary conditions. These should be defined on the primal space.
def all_boundary(x, on_boundary):
    return on_boundary

# Clamped everywhere on boundary.
bcs = [DirichletBC(U_L.R, Constant((0.0, 0.0)), all_boundary),
       DirichletBC(U_L.V, Constant(0.0), all_boundary)]

# The class MITC7 provides a helper utility to automatically construct
# boundary conditions on the reduced rotations and Lagrange multipliers.
bcs += U_L.auxilliary_dirichlet_conditions(bcs)

# Assemble in the normal way
A, b = assemble_system(a_b + a_s + R, L, bcs=bcs)

u_h = Function(U_L)
solver = LUSolver()
solver.solve(A, u_h.vector(), b)

theta_h, z_h = u_h.split()[U_L.primal_variables]

save_dir = "output/clamped_plate_mitc7_multipliers"
fields = {"theta": theta_h, "w": z_h}
for name,field in fields.iteritems():
    field.rename(name, name)
    field_file = XDMFFile("%s/%s.xdmf"%(save_dir,name))
    field_file.write(field)
