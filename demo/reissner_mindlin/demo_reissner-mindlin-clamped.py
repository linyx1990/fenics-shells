# Copyright (C) 2015 Jack S. Hale
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the out-of-plane Reissner-Mindlin equations on the
unit square with uniform transverse loading with fully clamped boundary conditions.

We use the Duran-Liberman projection operator to alleviate the problem of shear
locking in the Kirchhoff limit."""

from dolfin import *
from fenics_shells import *

# Create a mesh and define the full function space
# and the primal function space
mesh = UnitSquareMesh(32, 32)
mesh.init()

element = MixedElement([VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 1),
                        FiniteElement("N1curl", triangle, 1),
                        FiniteElement("N1curl", triangle, 1)])

# For convenience, we provide the helper function DuranLibermanSpace to set up
# scratch in this script.
#U = DuranLibermanSpace(mesh)
U = ProjectedFunctionSpace(mesh, element, num_projected_subspaces=2)

U_F = U.full_space
U_P = U.projected_space

u_ = Function(U_F)
theta_, w_, R_gamma_, p_ = split(u_)
u = TrialFunction(U_F)
u_t = TestFunction(U_F)

# Material parameters
E = Constant(10920.0)
nu = Constant(0.3)
kappa = Constant(5.0/6.0)
t = Constant(0.001)

# Bending energy
psi_b = psi_M(k(theta_), E=E, nu=nu, t=t)
L_b = psi_b*dx
# Bending residual
F_b = derivative(L_b, u_, u_t)
# Bending Jacobian
a_b = derivative(F_b, u_, u)

# Shear energy
psi_s = psi_T(R_gamma_, E=E, nu=nu, t=t, kappa=kappa)
L_s = psi_s*dx
# Shear residual
F_s = derivative(L_s, u_, u_t)
# Shear Jacobian
a_s = derivative(F_s, u_, u)

# Projection
L_R = inner_e(gamma(theta_, w_) - R_gamma_, p_)
F_R = derivative(L_R, u_, u_t)
a_R = derivative(F_R, u_, u)

a = a_b + a_s + a_R

theta, w, R_gamma, p = split(u)
theta_t, w_t, R_gamma_t, p_t = split(u_t)

# Loading residual
f = Constant(1.0)
L = inner(f*t**3, w_t)*dx

# By passing U_P as the first argument to assemble, we explicitly state that we
# want to assemble a Matrix or Vector from the forms on the projected part of the
# FunctionSpace.
A, b = assemble(U_P, a, L)

def all_boundary(x, on_boundary):
    return on_boundary

bcs = [DirichletBC(U, Constant((0.0, 0.0, 0.0)), all_boundary)]

for bc in bcs:
    bc.apply(A, b)

u_p_ = Function(U_P)
solver = LUSolver("mumps")
solver.solve(A, u_p_.vector(), b)

reconstruct_full_space(u_, u_p_, a, L)

save_dir = "output/reissner-mindlin-clamped"
theta, w, R_gamma, p = u_.split()
fields = {"theta": theta, "w": w, "R_gamma": R_gamma, "p": p}
for name, field in fields.iteritems():
    field.rename(name, name)
    field_file = XDMFFile("%s/%s.xdmf" % (save_dir, name))
    field_file.write(field)
