# Copyright (C) 2015 Jack S. Hale
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the out-of-plane Reissner-Mindlin equations on the
unit square with uniform transverse loading with simply supported boundary conditions.

We use the Duran-Liberman projection operator to alleviate the problem of shear
locking in the Kirchhoff limit."""

from dolfin import *
from fenics_shells import *

# Create a mesh and define the full function space
# and the primal function space
mesh = UnitSquareMesh(128, 128)

from fenics_shells.analytical.simply_supported import Displacement
w_e = Displacement(t=0.0001, nu=0.3, E=10920.0, p=0.0001**3)

# Material parameters
E = Constant(10920.0)
nu = Constant(0.3)
kappa = Constant(5.0/6.0)
t = Constant(0.0001)

element = MixedElement([VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 1),
                        FiniteElement("N1curl", triangle, 1),
                        FiniteElement("N1curl", triangle, 1)])

U = ProjectedFunctionSpace(mesh, element, num_projected_subspaces=2)
U_F = U.full_space
U_P = U.projected_space

u_ = Function(U_F)
theta_, w_, R_gamma_, p_ = split(u_)
u = TrialFunction(U_F)
u_t = TestFunction(U_F)

# Bending energy
psi_b = psi_M(k(theta_), E=E, nu=nu, t=t)
L_b = psi_b*dx
F_b = derivative(L_b, u_, u_t)
a_b = derivative(F_b, u_, u)

# Shear energy
psi_s = psi_T(R_gamma_, E=E, nu=nu, t=t, kappa=kappa)
L_s = psi_s*dx
F_s = derivative(L_s, u_, u_t)
a_s = derivative(F_s, u_, u)

# MITC Projection
L_R = inner_e(gamma(theta_, w_) - R_gamma_, p_)
F_R = derivative(L_R, u_, u_t)
a_R = derivative(F_R, u_, u)

a = a_b + a_s + a_R

theta, w, R_gamma, p = split(u)
theta_t, w_t, R_gamma_t, p_t = split(u_t)

f = Constant(1.0)
L = inner(f*t**3, w_t)*dx

A, b = assemble(U_P, a, L)

# Now boundary conditions. These should be defined on the primal space.
def all_boundary(x, on_boundary):
    return on_boundary

def left(x, on_boundary):
    return on_boundary and near(x[0], 0.0)

def right(x, on_boundary):
    return on_boundary and near(x[0], 1.0)

def bottom(x, on_boundary):
    return on_boundary and near(x[1], 0.0)

def top(x, on_boundary):
    return on_boundary and near(x[1], 1.0)

# Simply supported boundary conditions.
bcs = [DirichletBC(U.sub(1), Constant(0.0), all_boundary),
       DirichletBC(U.sub(0).sub(0), Constant(0.0), top),
       DirichletBC(U.sub(0).sub(0), Constant(0.0), bottom),
       DirichletBC(U.sub(0).sub(1), Constant(0.0), left),
       DirichletBC(U.sub(0).sub(1), Constant(0.0), right)]

for bc in bcs:
    bc.apply(A, b)

u_h = Function(U)
solver = LUSolver("mumps")
solver.solve(A, u_h.vector(), b)

theta_h, w_h = u_h.split()
w_e_h = project(w_e, FunctionSpace(mesh, "CG", 1))
print "Numerical displacement at centre: %.4e" % w_h((0.5, 0.5))
print "Analytical displacement at centre: %.4e" % w_e_h((0.5, 0.5))

save_dir = "output/reissner-mindlin-simply-supported"
fields = {"w": u_h, "theta_h": theta_h}
for name, field in fields.iteritems():
    field.rename(name, name)
    field_file = XDMFFile("%s/%s.xdmf" % (save_dir, name))
    field_file.write(field)
