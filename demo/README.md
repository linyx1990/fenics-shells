You can run all of the `demo_*.py` files in the subdirectories as tests using
the commands:

    source sourceme.sh
    py.test
