# Copyright (C) 2015 Matteo Brunetti
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the von Karman equations on the unit square with
uniform transverse loading with fully clamped boundary conditions.

We use the Continuous/Discontinuous Galerkin formulation to exploit standard
Lagrangian elements for the transverse displacement discretization.
"""

from dolfin import *
from fenics_shells import *

# Create a mesh and define the function space
mesh = UnitSquareMesh(15, 15)

# Problem space (in-plane and transverse displacement)
# TODO: Update fenics_shells/kirchhoff_love/function_spaces.py:class ContinuousDiscontinuous(df.FunctionSpace)
element = MixedElement([VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 2)]) 

U = FunctionSpace(mesh, element)

# Trial and Test function
u = TrialFunction(U)
u_t = TestFunction(U)
u_ = Function(U)
(v, w) = split(u)

# Material parameters
E = Constant(1.0)
nu = Constant(0.3)
t = Constant(1E-4)

# Boundary conditions
def all_boundary(x, on_boundary):
    return on_boundary

bcs = [DirichletBC(U, Constant((0.0, 0.0, 0.0)), all_boundary)]
bcs_theta = [DirichletBC(U.sub(1), Constant((0.0)), all_boundary)]

# stabilization parameter
alpha = cdg_stabilization(E, t)

# Kinematics
theta = kirchhoff_love_theta(w)
e = von_karman_e(v, theta)
k = k(theta)

# Membrane and Bending elastic energy density
psi_b = psi_M(k, E=E, nu=nu, t=t)
Pi_b = psi_b*dx
psi_m = psi_N(e, E=E, nu=nu, t=t)
Pi_m = psi_m*dx

# Discontinuos contribution to the elastic energy density
# TODO: the moment should be called as a function
D = (E*t**3)/(12.0*(1.0 - nu**2)) # bending stiffness
M = D*((1.0 - nu)*k + nu*tr(k)*Identity(2)) # bending stress tensor
# TODO: fenics_shells/fem/CDG.py:def cdg_energy() should be updated to avoid  dx[meshfunction] warning
Pi_cdg = cdg_energy(theta, M, alpha, mesh, bcs_theta)

# Uniform transverse loading and external work
f = 10**5*t**3
L = f*w*dx

# Total energy
Pi = Pi_b + Pi_m + Pi_cdg - L

# Residuals and Jacobian
Pi = action(Pi, u_)
F = derivative(Pi, u_, u_t)
J = derivative(F, u_, u)

# Initial guess
init = Function(U)
u_.assign(init)

problem = NonlinearVariationalProblem(F, u_, bcs, J = J)
solver = NonlinearVariationalSolver(problem)
solver.parameters.nonlinear_solver = 'snes'
solver.parameters.snes_solver.linear_solver =  'umfpack'

solver.solve()
v_h, w_h = u_.split()
plot(w_h, interactive=True)

save_dir = "output/vonkarman-cdg-clamped"
fields = {"v": v_h, "w": w_h}
for name,field in fields.iteritems():
    field.rename(name, name)
    field_file = XDMFFile("%s/%s.xdmf"%(save_dir,name))
    field_file.write(field)
