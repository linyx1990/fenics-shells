# Copyright (C) 2015 Matteo Brunetti
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the von Karman equations on a circular plate with
lenticular cross section. The plate is free on the boundary. Analytical
solution can be found in the paper:

E. H. Mansfield, "Bending, buckling and curling of a heated elliptical plate."
Proceedings of the Royal Society of London A: Mathematical, Physical and
Engineering Sciences.  Vol. 288. No. 1414. The Royal Society, 1965.

We use the Continuous/Discontinuous Galerkin formulation to exploit standard
Lagrangian elements for the transverse displacement discretization
"""

import numpy as np

from dolfin import *
from fenics_shells import *

try:
    import matplotlib.pyplot as plt
except ImportError:
    raise ImportError("matplotlib is required to run this demo.")

try:
    import mshr
except ImportError:
    raise ImportError("mshr is required to run this demo.")

# Create a mesh and define the function space
radius = 1.0
n_div = 12
centre = Point(0.,0.)
domain_area = np.pi*radius**2
geom = mshr.Circle(centre, radius)
mesh = mshr.generate_mesh(geom, n_div)
h_max = mesh.hmax()
mesh.init()

# Problem space (in-plane and transverse displacement)
# TODO: Update fenics_shells/kirchhoff_love/function_spaces.py:class ContinuousDiscontinuous(df.FunctionSpace)
element = MixedElement([VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 2)]) 

U = FunctionSpace(mesh, element)

# Trial and Test function
u = TrialFunction(U)
u_t = TestFunction(U)
u_ = Function(U)
(v, w) = split(u)

# Fix the value in the centre to eliminate the nullspace
def center(x,on_boundary):
    return x[0]**2 + x[1]**2 < (0.5*h_max)**2

bc_v = DirichletBC(U.sub(0), Constant((0.0,0.0)), center, method="pointwise")
bc_w = DirichletBC(U.sub(1), Constant(0.0), center, method="pointwise")
bcs = [bc_v, bc_w]

# Geometric and material parameters (thickness)
E = Constant(1.0)
nu = Constant(0.3)
t = Constant(1E-2)

# Defines the lenticular thinning of the plate
th_f = Expression('(1.0 - (x[0]*x[0])/(R*R) - (x[1]*x[1])/(R*R))', R=radius)

# Stabilization parameter
alpha = cdg_stabilization(E, t)

# Target inelastic curvature
k_T = as_tensor(Expression((("1.0*c","0.0*c"),("0.0*c","0.975*c")), c=1.0))

# Kinematics
theta = kirchhoff_love_theta(w)
e = von_karman_e(v, theta)
k_ef = k(theta) - k_T

# Membrane and bending elastic energy density
# We subtract the inelastic curvature from the curvature
psi_b = psi_M(k_ef, E=E, nu=nu, t=t)
Pi_b = psi_b*dx
psi_m = psi_N(e, E=E, nu=nu, t=t)
Pi_m = psi_m*dx

# Discontinuos contribution to the elastic energy density
# TODO: the moment should be called as a function
D = (E*t**3)/(12.0*(1.0 - nu**2)) # bending stiffness
M = D*((1.0 - nu)*k_ef + nu*tr(k_ef)*Identity(2)) # bending stress tensor
# TODO: fenics_shells/fem/CDG.py:def cdg_energy() should be updated to avoid  dx[meshfunction] warning
Pi_cdg = cdg_energy(theta, M, alpha, mesh)

# Total energy
Pi = Pi_m + Pi_b + Pi_cdg

# Residuals and Jacobian
Pi = action(Pi, u_)
F = derivative(Pi, u_, u_t)
J = derivative(F, u_, u)

# Initial guess
init = Function(U)
u_.assign(init)

# Solver settings
problem = NonlinearVariationalProblem(F, u_, bcs, J = J)
solver = NonlinearVariationalSolver(problem)
solver.parameters.nonlinear_solver = 'snes'
solver.parameters.snes_solver.linear_solver =  'umfpack'
solver.parameters.snes_solver.maximum_iterations =  50

# Solution
kx = []
ky = []
kxy = []
ls_load = []

# Analytical critical inelastic curvature (see E. H. Mansfield, 1962)
c_cr = 0.0516
loadings = np.linspace(0.0, 1.5*c_cr, 50)

# Post-processing
v_h, w_h = u_.split(deepcopy=True)

# plt.xlabel('load')
# plt.ylabel('Average curvatures')
# plt.ion()
# plt.grid()
# plt.show()

for i in loadings:
    k_T.c = i
    solver.solve()

    v_h, w_h = u_.split(deepcopy=True)
    K_h = project(grad(grad(w_h)), TensorFunctionSpace(mesh, 'DG', 0))
    Kxx = assemble(K_h[0,0]*dx)/domain_area
    Kyy = assemble(K_h[1,1]*dx)/domain_area
    Kxy = assemble(K_h[0,1]*dx)/domain_area
    ls_load.append(i)
    kx.append(Kxx)
    ky.append(Kyy)
    kxy.append(Kxy)
    # plt.plot(ls_load, kx,'k',linewidth=3.5)
    # plt.plot(ls_load, ky,'r',linewidth=3.5)
    # plt.xlabel('Temperature gradient', fontsize=15)
    # plt.ylabel('Average curvatures', fontsize=15)
    # plt.draw()