#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

"""This demo program solves the Reissner-Mindlin-von-Karman equations on the
unit square with uniform transverse loading and fully clamped boundary
conditions.

We use the Duran-Liberman formulation with a projected assemble to exploit
standard Lagrangian elements and cure the shear-locking problem.
"""
from dolfin import *
from fenics_shells import *

# Define the mesh
mesh = UnitSquareMesh(15, 15)

# In-plane displacements, rotations, out-of-plane displacements and shear strains
element = MixedElement([VectorElement("Lagrange", triangle, 1),
                        VectorElement("Lagrange", triangle, 2),
                        FiniteElement("Lagrange", triangle, 1),
                        FiniteElement("N1curl", triangle, 1),
                        FiniteElement("N1curl", triangle, 1)])

U = ProjectedFunctionSpace(mesh, element, num_projected_subspaces=2)
U_F = U.full_space
U_P = U.projected_space

u_ = Function(U_F)
v_, theta_, w_, R_gamma_, p_ = split(u_)
u = TrialFunction(U_F)
u_t = TestFunction(U_F)

# Define the material parameters
E = Constant(1.0)
nu = Constant(0.3)
kappa = Constant(5.0/6.0)
t = Constant(1E-4)

# Bending energy.
psi_b = psi_M(k(theta_), E=E, nu=nu, t=t)

# Von-Karman membrane energy.
e = von_karman_e(v_, grad(w_))
psi_m = psi_N(e, E=E, nu=nu, t=t)

# Reissner-Mindlin shear energy. 
psi_s = psi_T(R_gamma_, E=E, nu=nu, t=t, kappa=kappa)

# Projection
L_R = inner_e(gamma(theta_, w_) - R_gamma_, p_)

# External work
f = Constant(10**5*t**3)
W_ext = f*w_*dx

# Stored elastic strain energy
psi = psi_b + psi_m + psi_s

# Lagrangian
L = psi*dx + L_R - W_ext 
F = derivative(L, u_, u_t)
J = derivative(F, u_, u)

def all_boundary(x, on_boundary):
    return on_boundary

bcs = [DirichletBC(U_P, Constant((0.0, 0.0, 0.0, 0.0, 0.0)), all_boundary)]

u_p_  = Function(U_P)
problem = ProjectedNonlinearProblem(U_P, F, u_, u_p_, J=J, bcs=bcs)
solver = NewtonSolver()
solver.solve(problem, u_p_.vector())

v_h, theta_h, w_h  = u_p_.split()

save_dir = "output/vonkarman-mitc-clamped"
fields = {"theta": theta_h, "v": v_h, "w": w_h}
for name,field in fields.iteritems():
    field.rename(name, name)
    field_file = XDMFFile("%s/%s.xdmf"%(save_dir,name))
    field_file.write(field)
