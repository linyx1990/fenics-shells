# FEniCS-Shells
## A UFL and DOLFIN based library for modelling thin structures

### Description

FEniCS-Shells is an open-source library that provides a wide range of thin
structural models (beams, plates and shells) expressed in the Unified Form
Language (UFL) of the [FEniCS Project](http://fenicsproject.org). 

### Features

FEniCS-Shells currently includes implementations of the following structural
models:

* Kirchhoff-Love plates,
* Reissner-Mindlin plates, 
* von-Kármán shallow shells,
* Reissner-Mindlin-von-Kármán shallow shells,  
* non-linear Naghdi shells with exact geometry.

Additionally, the following models are under active development:

* linear and non-linear Timoshenko beams,
* non-linear Mardare-Naghdi shells with exact geometry.

We are using a variety of finite element numerical techniques including:

* MITC reduction operators,
* Discontinuous Galerkin methods,
* Reduced integration techniques.

### Citing

We are currently preparing a paper that describes FEniCS-Shells in full. In
the meantime, please consider citing the following figshare DOI if you
find the examples in FEniCS-Shells useful for your paper:

https://dx.doi.org/10.6084/m9.figshare.4291160
   
    @misc{hale_fenics-shells_2016,
	    title = {{FEniCS}-{Shells}},
	    url = {https://figshare.com/articles/FEniCS-Shells/4291160},
	    abstract = {FEniCS-Shells is an open-source library (LGPLv3) that provides a wide range 
                    of thin structural models (beams, plates and shells) solved using the 
                    finite element method and expressed in the Unified Form Language (UFL) 
                    of the FEniCS Project.},
	    author = {Hale, Jack S. and Brunetti, Matteo and Bordas, Stéphane P.A. and Maurini, Corrado},
	    month = dec,
	    year = {2016},
	    doi = {10.6084/m9.figshare.4291160},
	    keywords = {FEniCS, Locking, MITC, PDEs, Python, Shells, thin structures},
    }

along with the appropriate general [FEniCS citations](http://fenicsproject.org/citing).

### Getting started

1. Install FEniCS by following the instructions
   [here](http://fenicsproject.org/download). We recommend using the
   `fenicsproject` script alongside [Docker](http://docker.com) to run FEniCS
   which will give you an optimal up-to-date and precompiled version of FEniCS
   that will run on any platform.
 
2. Then, clone fenics-shells using the command:

        git clone https://bitbucket.org/unilucompmech/fenics-shells.git

3. Start a docker container using the `fenicsproject` script:

        cd fenics-shells
        fenicsproject create fenics-shells
        fenicsproject start fenics-shells

3. You should now have a shell inside a container with FEniCS installed. Try
   out an example:

        cd demo
        touch sourceme.sh
        cd reissner_mindlin
        python demo_reissner-mindlin-clamped.py

   The resulting deformations are written to the directory `output/` which
   will be shared with the host machine. These files can be opened using
   [Paraview](http://www.paraview.org/). 

### Contributing

We are always looking for contributions and help with fenics-shells. If you
have ideas, nice applications or code contributions then we would be happy to
help you get them included. We ask you to follow the [FEniCS Project git
workflow](https://bitbucket.org/fenics-project/dolfin/wiki/Git%20cookbook%20for%20FEniCS%20developers).

### Issues and Support

Please use the [bugtracker](http://bitbucket.org/unilucompmech/fenics-shells)
to report any issues.

For support or questions please email [jack.hale@uni.lu](mailto:jack.hale@uni.lu).

### Authors (alphabetical)

Matteo Brunetti, Université Pierre et Marie Curie, Paris.

Jack S. Hale, University of Luxembourg, Luxembourg.

Corrado Maurini, Université Pierre et Marie Curie, Paris.

### License 

fenics-shells is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with fenics-shells.  If not, see <http://www.gnu.org/licenses/>.