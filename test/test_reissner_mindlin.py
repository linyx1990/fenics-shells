# -*- coding: utf-8 -*-

# Copyright (C) 2015 Jack S. Hale
#
# This file is part of fenics-shells.
#
# fenics-shells is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# fenics-shells is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with fenics-shells. If not, see <http://www.gnu.org/licenses/>.

import pytest

import numpy as np
import dolfin

from dolfin import *
from fenics_shells import *


def _analytical_lovadina(nx, function_space_type, t):
    """For given number of divisions, and an element, solve the Lovadina
    clamped plate problem for a given thickness parameter t.
    """
    mesh = UnitSquareMesh(nx, nx)
    U = function_space_type(mesh)

    U_F = U.full_space
    U_P = U.projected_space

    def all_boundary(x, on_boundary):
        return on_boundary

    bc_R = DirichletBC(U_P.sub(0), Constant((0.0, 0.0)), all_boundary)
    bc_V_3 = DirichletBC(U_P.sub(1), Constant((0.0)), all_boundary)
    bcs = [bc_R, bc_V_3]
    
    E = 10920.0
    kappa = 5.0/6.0
    nu = 0.3

    from fenics_shells.analytical.lovadina_clamped import Loading, Rotation, Displacement
    theta_e = Rotation()
    w_e = Displacement(t=10**-4, nu=nu)
    f = Loading(t=t, E=E, nu=nu)

    u = TrialFunction(U_F)
    u_t = TestFunction(U_F)
    u_ = Function(U_F)
    theta_, w_, R_gamma_, p_ = split(u_)

    E = Constant(E)
    nu = Constant(nu)
    t = Constant(t) 
    kappa = Constant(kappa)
    
    # Elastic energy density
    psi = psi_M(k(theta_), E=E, nu=nu, t=t) \
        + psi_T(R_gamma_, E=E, nu=nu, t=t, kappa=kappa) \
    # Elastic energy
    L_el = psi*dx

    # External work
    W_ext = inner(f, w_)*dx

    # Reduction operator
    L_R = inner_e(gamma(theta_, w_) - R_gamma_, p_)

    L = L_el + L_R - W_ext
    F = derivative(L, u_, u_t)
    J = derivative(F, u_, u)
 
    A, b = assemble(U_P, J, rhs(F))
    for bc in bcs:
        bc.apply(A, b)

    u_p_ = Function(U_P)
    
    solver = LUSolver("mumps")
    solver.solve(A, u_p_.vector(), b)

    theta_h, w_h = u_p_.split()
    
    result = {}
    result['hmax'] = mesh.hmax()
    result['hmin'] = mesh.hmin()
    result['theta_l2'] = errornorm(theta_e, theta_h, norm_type='l2')/norm(theta_h, norm_type='l2')
    result['theta_h1'] = errornorm(theta_e, theta_h, norm_type='h1')/norm(theta_h, norm_type='h1')
    result['w_l2'] = errornorm(w_e, w_h, norm_type='l2')/norm(w_h, norm_type='l2')
    result['w_h1'] = errornorm(w_e, w_h, norm_type='h1')/norm(w_h, norm_type='h1')
    result['hmax'] = mesh.hmax()
    result['dofs'] = U.dim()
    
    return result


def _runner(element, norms, expected_convergence_rates):
    """Given an element and norms, compare the computed convergence
    rate and the expected convergence rate and assert that the former
    is greater than the latter."""
    nxs = [16, 32, 64]
    # Doesn't make too much sense to run a convergence test 
    # with only one evaluation.
    assert(len(nxs) > 1)
    
    # TODO: iterate on t as well to ensure no locking
    results = []
    t = 1.0E-4
    results = []
    for nx in nxs:
        result = _analytical_lovadina(nx, element, t)
        results.append(result)

    for norm, expected_convergence_rate in zip(norms, expected_convergence_rates):
        hs = np.array([x['hmax'] for x in results])
        errors = np.array([x[norm] for x in results])
        
        actual_convergence_rate = np.polyfit(np.log(hs), np.log(errors), 1)[0]
        err_msg = "Convergence rate in norm %s = %.3f, expected %.3f" % \
                    (norm, actual_convergence_rate, expected_convergence_rate)

        assert actual_convergence_rate >= expected_convergence_rate, err_msg


def test_duran_liberman():
    """Run test for DuranLiberman element type"""
    norms = ['theta_l2', 'theta_h1', 'w_l2', 'w_h1']
    #expected_convergence_rates = [2.0, 1.0, 2.0, 1.0]
    expected_convergence_rates = [1.8, 0.8, 1.8, 0.8]

    _runner(DuranLibermanSpace, norms, expected_convergence_rates)       

@pytest.mark.skipif(dolfin.__version__ != "1.5.0", reason="Calculation of dual \
                    DOFs of enriched elements disabled in DOLFIN >1.5.0")  
def test_mitc7():
    """Run test for MITC7 element type"""
    norms = ['theta_l2', 'theta_h1', 'w_l2', 'w_h1']
    #expected_convergence_rates = [3.0, 2.0, 3.0, 2.0]
    expected_convergence_rates = [2.8, 1.8, 2.8, 1.8]

    _runner(MITC7, norms, expected_convergence_rates)   
